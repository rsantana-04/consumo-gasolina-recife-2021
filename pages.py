class BasePage:

    def __init__(self, driver):
        self.driver = driver


class LoginPage(BasePage):

    def login(self, user, password):
        self.driver.find_element_by_name("username").send_keys(user)
        self.driver.find_element_by_name("password").send_keys(password)
        self.driver.find_element_by_name("action").click()


class MainPage(BasePage):

    def access_menu(self, menu_option):
        self.driver.find_element_by_xpath("//h6[contains(.,'" + menu_option + "')]").click()


class MessagePage(BasePage):

    def success_visible(self):
        return self.driver.find_element_by_css_selector(".toast.success-toast").is_displayed()

    def required_visible(self):
        return self.driver.find_element_by_css_selector(".errornote").is_displayed()


class SupplyListPage(BasePage):

    def add_button(self):
        self.driver.find_element_by_xpath("//a[contains(.,'add')]").click()


class SupplyAddPage(BasePage):
    def add(self, km, liters, value, date, hour):
        self.driver.find_element_by_name("km_supply").send_keys(km)
        self.driver.find_element_by_name("quantity_liters").send_keys(liters)
        self.driver.find_element_by_name("value").send_keys(value)
        self.driver.find_element_by_name("date_supply_0").send_keys(date)
        self.driver.find_element_by_name("date_supply_1").send_keys(hour)
        self.driver.find_element_by_css_selector(".open-actions > button").click()
